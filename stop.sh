#!/usr/bin/env bash

set -e


CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [[ $# -eq 0 ]] ;
then
    shopt -u dotglob
    SERVICES=( $(find . -type f -mindepth 2 -maxdepth 2 -name 'pom.xml' | rev | cut -d '/' -f 2 | rev) )
else
    SERVICES=( $@ )
fi

for ARGUMENT in "${SERVICES[@]}"
do
    PID=`jcmd | grep ${ARGUMENT%/} | cut -d ' ' -f 1`
    if [[ ! -z $PID ]];
    then
        kill -9 $PID
        echo "Service ${ARGUMENT%/} was terminated"
    fi
done