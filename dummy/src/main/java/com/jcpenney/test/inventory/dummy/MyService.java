package com.jcpenney.test.inventory.dummy;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class MyService {

    @Autowired
    private NumbersApi numbersApi;

    @HystrixCommand(fallbackMethod = "popaFallback")
    public List<String> popa (int number) {
        return numbersApi.getStrings(number);
    }

    public List<String> popaFallback (int number) {
        return Collections.singletonList("defect");
    }
}
