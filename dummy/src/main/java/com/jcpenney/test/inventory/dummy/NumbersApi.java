package com.jcpenney.test.inventory.dummy;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient("numbers")
public interface NumbersApi {

    @RequestMapping
    List<String> getStrings(@RequestParam("numbers") int numbers);
}
