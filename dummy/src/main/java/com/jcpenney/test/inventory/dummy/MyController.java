package com.jcpenney.test.inventory.dummy;


import com.netflix.appinfo.EurekaInstanceConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
class MyController {

    @Autowired
    private EurekaInstanceConfig eurekaInstanceConfig;

    @Autowired
    private MyService myService;

    @RequestMapping("/{name}")
    public String greeting(@PathVariable("name") String name, HttpServletResponse response) {
        response.setHeader("X-INSTANCE-ID", eurekaInstanceConfig.getInstanceId());
        return "Hello " + name;
    }

    @RequestMapping
    public List<String> numbers(@RequestParam("numbers") int number) {
        return myService.popa(number);
    }

}
