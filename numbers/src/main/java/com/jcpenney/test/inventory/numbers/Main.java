package com.jcpenney.test.inventory.numbers;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

@RestController
@EnableEurekaClient
@SpringBootApplication
public class Main {

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @RequestMapping
    public List<String> getStrings(@RequestParam("numbers") int numbers) {
        if (numbers <= 0) {
            throw new RuntimeException("numbers <= 0");
        }
        return IntStream.range(0, numbers).boxed().map(it -> "Hello world").collect(toList());
    }
}
