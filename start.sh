#!/usr/bin/env bash

set -e


CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
mkdir -p $CURRENT_DIR/logs


start_service() {
    local FOLDER=$1

    if [ $FOLDER = "configserver" ];
    then
        ( cd $FOLDER ; java -jar ./target/*.jar > $CURRENT_DIR/logs/$FOLDER.log &)
    else
        java -jar $FOLDER/target/*.jar > $CURRENT_DIR/logs/$FOLDER.log &
    fi

    echo "Service $FOLDER started"
}

if [[ $# -eq 0 ]] ;
then
    shopt -u dotglob
    SERVICES=( $(find . -type f -mindepth 2 -maxdepth 2 -name 'pom.xml' | rev | cut -d '/' -f 2 | rev) )
else
    SERVICES=( $@ )
fi

for ARGUMENT in "${SERVICES[@]}"
do
    start_service ${ARGUMENT%/}
done
